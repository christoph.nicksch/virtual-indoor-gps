## Load Libraries
from scipy.spatial.transform import Rotation as R
import numpy as np
import matplotlib.pyplot as plt
import copy
from matplotlib import colors as colors
from math import pi, cos, sin

## Define Functions
# function to calculate intersection for sets of Origins P0 and LOS n
def intersect(P0,n):

    n = n/np.linalg.norm(n,axis=1)[:,np.newaxis]
    # generate the array of all projectors
    projs = np.eye(n.shape[1]) - n[:,:,np.newaxis]*n[:,np.newaxis]  # I - n*n.T
    # see fig. 1

    # generate R matrix and q vector
    R = projs.sum(axis=0)
    q = (projs @ P0[:,:,np.newaxis]).sum(axis=0)

    # solve the least squares problem for the
    # intersection point p: Rp = q
    p = np.linalg.lstsq(R,q,rcond=None)[0]

    return p
# function to print ProgressBar in cli
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()
# function to reshape x = flattened[eulertransformations, LOS_tx] -> eulerTransformation, LOS_tx
def flatToInput(numberTransmitter, x):
    eulerTransformations = np.reshape(x[0:numberTransmitter * 6], newshape=(numberTransmitter, 6))
    LOS_tx = np.reshape(x[numberTransmitter*6:len(x)], newshape=(numberTransmitter, 3))
    return eulerTransformations, LOS_tx
# function to calculate MU based on all eulerTransformations, LOS, standarduncertainties
def model(eulerTransformations, LOS_tx, standardUncertainty_calibration, standardUncertainty_LOS):
    # get number of transmitters
    numberOfTransmitters = len(eulerTransformations+1)

    #reshape input (eulerTransformations and LOS_Tx) to one flat array in order to iterate easily
    x = np.concatenate([eulerTransformations.flatten(), LOS_tx.flatten()])
    #reshape standarduncertainties to one flat array (like x)
    u_i_calibration = []
    u_i_LOS = []
    for i in range(numberOfTransmitters):
        u_i_calibration.append(standardUncertainty_calibration)
    for i in range(numberOfTransmitters):
        u_i_LOS.append(standardUncertainty_LOS)
    u_i_calibration = np.array(u_i_calibration)
    u_i_LOS = np.array(u_i_LOS)
    u_i = np.concatenate([u_i_calibration.flatten(), u_i_LOS.flatten()])

    deltaList = []
    # iterate for over all input values
    for i in range(len(u_i)):
        # for i standarduncertainty has to be added or subtracted for numerical differentiation
        x_1 = copy.deepcopy(x)
        x_1[i] = x_1[i] + u_i[i]
        x_2 = copy.deepcopy(x)
        x_2[i] = x_2[i] - u_i[i]
        # get the flat array back to eulerTransformation and LOS_tx
        eulerTransformations1, LOS_tx1 = flatToInput(numberOfTransmitters, x_1)
        eulerTransformations2, LOS_tx2 = flatToInput(numberOfTransmitters, x_2)

        # calculate LOS in world coordinates
        origins1 = eulerTransformations1[:,0:3]
        origins2 = eulerTransformations2[:, 0:3]
        rots1 = eulerTransformations1[:,3:6]
        rots2 = eulerTransformations2[:,3:6]
        LOS_world1 = []
        LOS_world2 = []
        for ind in range(len(rots1)):
            r1 = R.from_euler(seq='ZYX', angles=rots1[ind, :])
            r2 = R.from_euler(seq='ZYX', angles=rots2[ind, :])
            LOS_world1.append(np.dot(r1.as_matrix(), LOS_tx1[ind, :]))
            LOS_world2.append(np.dot(r2.as_matrix(), LOS_tx2[ind, :]))
        LOS_world1 = np.array(LOS_world1)
        LOS_world2 = np.array(LOS_world2)

        # get detectorposition with numerical intersection
        det1 = intersect(origins1, LOS_world1)
        det2 = intersect(origins2, LOS_world2)
        delta = np.square(det1-det2)
        deltaList.append(delta)

    deltaList = np.squeeze(deltaList)

    u_x = np.sqrt(0.25 * np.sum(deltaList[:, 0]))
    u_y = np.sqrt(0.25 * np.sum(deltaList[:, 1]))
    u_z = np.sqrt(0.25 * np.sum(deltaList[:, 2]))
    # calculate combined standard uncertainty
    u_c = np.linalg.norm([u_x, u_y, u_z])

    return u_c*1000


# input euler Transformations [x,y,z,ex,ey,ez] of transmitters (angles in radians)
eulerTransformations = np.array([
    [2,-2,0,0,0,0],
    [2,2,7,0,0,0],
    [-2,2,1,0,0,0],
    [-2,-2,4,0,0,0]])
# input z position receiver
z_ls = 3


# heat map size is calculated automatically
max = round(np.max(np.abs(eulerTransformations[:,0:2]))*1.1, 2)
# define grid; last element (number) is resolution
xs = np.linspace(-max, max, 40)
ys = np.linspace(-max, max, 40)

# create meshgrid
x_1, y_1 = np.meshgrid(xs, ys)

# standard uncertainty of LOS and eulerTransformations
standardUncertainty_LOS = np.array([0.0000072401800, 0.000006262813, 0.000005203829064])
numberOfTransmitters = len(eulerTransformations)
if numberOfTransmitters>6:
	numberOfTransmitters=6
elif numberOfTransmitters<3:
	numberOfTransmitters=3
standardUncertainty_calibration = np.array([
	-0.00032794 * numberOfTransmitters + 0.002154307,
	-0.00030133 * numberOfTransmitters + 0.002023659,
	-0.00012578 * numberOfTransmitters + 0.001102549,
	-0.00014444 * numberOfTransmitters + 0.000933838,
	-0.00004051 * numberOfTransmitters + 0.000357961,
	-0.00004123 * numberOfTransmitters + 0.000359153])


i = 0
# z = uncertainty
z = np.empty(len(xs) * len(ys))
printProgressBar(0, (len(xs)*len(ys)), prefix = 'Progress:', suffix = 'Complete', length = 50)
# iterate over each meshgrid element (x,y)
for x_ls in xs:
	for y_ls in ys:
		LOS_tx = []
        # calculate LOS for each transmitter
		for transmitter in eulerTransformations:
            # calculate LOS in world coordinates
			LOS_world = np.array([x_ls, y_ls, z_ls]) - np.array(transmitter[0:3])
            # transform LOS to transmitter coordinates
			r = R.from_euler(seq='ZYX', angles=transmitter[3:6])
			LOS_tx.append(np.dot(np.linalg.inv(r.as_matrix()), LOS_world))
		LOS_tx = np.array(LOS_tx)
        # calculate mu
		z[i] = model(eulerTransformations, LOS_tx, standardUncertainty_calibration, standardUncertainty_LOS)
		i += 1
		# printProgressBar(i, (len(xs)*len(ys)), prefix='Progress:', suffix='Complete', length=50)
		printProgressBar(i, (len(xs)*len(ys)), prefix='Progress:', suffix='Complete', length=50)

z.shape = x_1.shape

# plot heatmap
plt.figure()
plt.contourf(y_1, x_1, z, cmap=colors.LinearSegmentedColormap.from_list("", ["green", "yellow", "red"]), levels=30)
plt.xlabel('X-Achse [m]')
plt.ylabel('Y-Achse [m]')
clb = plt.colorbar()
clb.ax.set_title('u [mm]')
plt.scatter(x=eulerTransformations[:, 0], y=eulerTransformations[:, 1], c='black', s=120)
plt.gca().set_aspect('equal', adjustable='box')
#save figure to file
plt.savefig('uncertainty_heatmap.png', bbox_inches='tight')
